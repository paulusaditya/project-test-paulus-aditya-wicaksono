        function getQueryParam(param) {
        const urlParams = new URLSearchParams(window.location.search);
        return urlParams.get(param);
    }


    function setQueryParam(param, value) {
        const urlParams = new URLSearchParams(window.location.search);
        urlParams.set(param, value);
        window.history.replaceState({}, '', `${window.location.pathname}?${urlParams}`);
    }

    let pageSize = parseInt(getQueryParam('pageSize')) || 10; 
    let currentPage = parseInt(getQueryParam('page')) || 1; 
    let sortBy = getQueryParam('sort') || 'newest'; 

    document.getElementById('show-per-page').value = pageSize;
    document.getElementById('sort-by').value = sortBy;

    async function fetchData(pageNumber, pageSize) {
        const apiUrl = 'https://suitmedia-backend.suitdev.com/api/ideas';
        const params = {
            'page[number]': pageNumber,
            'page[size]': pageSize,
            'append[]': ['small_image', 'medium_image'],
            'sort': sortBy === 'newest' ? '-published_at' : 'published_at'
        };

        try {
            const response = await axios.get(apiUrl, { params });
            const data = response.data.data;
            const cardsContainer = document.getElementById('cards');
            
            cardsContainer.innerHTML = '';

            data.forEach(item => {
                const card = document.createElement('div');
                card.className = 'card';

                const img = document.createElement('img');
                img.loading = 'lazy'; // Set loading lazy
                if (item.small_image && item.small_image.length > 0) {
                    img.src = item.small_image[0].url; 
                } else {
                    img.src = 'placeholder.jpg'; 
                }
                img.alt = item.title;

                const description = document.createElement('div');
                description.className = 'description';

                const date = document.createElement('p');
                const publishedDate = new Date(item.published_at).toLocaleDateString('id-ID', {
                    day: 'numeric', month: 'long', year: 'numeric'
                });
                date.textContent = publishedDate;

                const title = document.createElement('h4');
                title.textContent = item.title;

                description.appendChild(date);
                description.appendChild(title);
                card.appendChild(img);
                card.appendChild(description);
                cardsContainer.appendChild(card);
            });

            updatePagination(pageNumber, pageSize);
        } catch (error) {
            console.error('Error fetching data:', error);
        }
    }

    function updatePagination(currentPage, pageSize) {
        const paginationContainer = document.getElementById('pagination');
        paginationContainer.innerHTML = '';

        const totalItems = 100;
        const totalPages = Math.ceil(totalItems / pageSize);

        for (let i = 1; i <= totalPages; i++) {
            const button = document.createElement('button');
            button.textContent = i;
            if (i === currentPage) {
                button.classList.add('active');
            }
            button.addEventListener('click', () => {
                currentPage = i;
                setQueryParam('page', currentPage);
                fetchData(currentPage, pageSize);
            });
            paginationContainer.appendChild(button);
        }
    }

    fetchData(currentPage, pageSize);


    document.getElementById('sort-by').addEventListener('change', function() {
        sortBy = this.value;
        currentPage = 1; 
        setQueryParam('sort', sortBy);
        setQueryParam('page', currentPage);
        fetchData(currentPage, pageSize);
    });


    document.getElementById('show-per-page').addEventListener('change', function() {
        pageSize = parseInt(this.value);
        currentPage = 1; 
        setQueryParam('pageSize', pageSize);
        setQueryParam('page', currentPage);
        fetchData(currentPage, pageSize);
    });