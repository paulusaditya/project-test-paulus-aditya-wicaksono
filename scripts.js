window.addEventListener('scroll', function() {
    const bannerImage = document.querySelector('.banner-image');
    let scrollPosition = window.pageYOffset;
    bannerImage.style.transform = 'translateY(' + scrollPosition * 0.5 + 'px) translateZ(-1px) scale(2)';
});

const showPerPageSelect = document.getElementById('show-per-page');
const sortBySelect = document.getElementById('sort-by');

showPerPageSelect.addEventListener('change', () => {
  // Handle perubahan nilai show per page
});

sortBySelect.addEventListener('change', () => {
  // Handle perubahan nilai sort by
});

let lastScrollTop = 0;
const header = document.getElementById('header');

window.addEventListener('scroll', function() {
    const scrollTop = window.pageYOffset || document.documentElement.scrollTop;

    if (scrollTop > lastScrollTop) {
        // Scroll down
        header.style.transform = 'translateY(-100%)';
    } else {
        // Scroll up
        header.style.transform = 'translateY(0)';
    }

    lastScrollTop = scrollTop;
});

document.getElementById('show-per-page').addEventListener('change', function() {
  const pageSize = this.value;
  fetchData(pageSize);
});
